#!/bin/sh

WORK_PATH=$(dirname $(readlink -f $0))
echo "WORK PATH $WORK_PATH"
VERSION=${WORK_PATH##*/}
echo "VERSION $VERSION"
WORK_PATH_WITHOUT_VERSION=`echo ${WORK_PATH%/*}`
echo "WORK_PATH_WITHOUT_VERSION $WORK_PATH_WITHOUT_VERSION"
SERVICE=`echo ${WORK_PATH_WITHOUT_VERSION##*/}`
echo "SERVICE $SERVICE"
SHUTDOWN_TIMEOUT=30

usage(){
  echo "_________________________________________"
  echo "Usage: run.sh ACTION"
}

argsNo=1
CURRENT_TIME=`date '+%Y-%m-%d :H:%M:S'`
MAX_HEAP="512m"
INITIAL_HEAP="256m"
JAVA_OPTS="-Xms$INITIAL_HEAP -Xmx$MAX_HEAP"

status(){
  pid=`ps -ef | grep $SERVICE | grep $VERSION | awk '{print $2}'`
  if [ -z "${pid}" ]; then
    echo "$SERVICE $VERSION is not running"
    return 1
  else
     echo "$SERVICE $VERSION is running with pid: [$pid]"
     return 0
  fi
}

start(){
  status
  if [ $? -eq "1" ]; then
    nohup java $JAVA_OPTS -cp . -jar $SERVICE-$VERSION.jar $VERSION &
    status
  fi
  return 0
}

stop(){
  status
  if [ $? -eq "0" ]; then
    kill $pid
    count=1
    while [ $count -le $SHUTDOWN_TIMOUT ]
    do
      status
      if [ $? -eq "1" ]; then
        echo "$SERVICE $VERSION is stopped"
        break
      fi
    done
    status
    if [ $? -eq "0" ]; then
      echo "$SERVICE $VERSION failed to stopped gracefully, hence kill forcefully"
      kill -9 $pid
      echo "$SERVICE $VERSION stopped"
    fi
  fi
  sleep 1
  return 0
}

restart(){
  stop
  sleep 2
  start
}

case "$1" in
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "status")
    status
    ;;
  "restart")
    restart
    ;;
  *)
    usage
    ;;
esac