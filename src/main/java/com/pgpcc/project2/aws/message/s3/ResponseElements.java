package com.pgpcc.project2.aws.message.s3;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "x-amz-request-id",
        "x-amz-id-2"
})
@Data
public class ResponseElements implements Serializable {

    private final static long serialVersionUID = -6304159793998073859L;
    @JsonProperty("x-amz-request-id")
    public String xAmzRequestId;
    @JsonProperty("x-amz-id-2")
    public String xAmzId2;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
