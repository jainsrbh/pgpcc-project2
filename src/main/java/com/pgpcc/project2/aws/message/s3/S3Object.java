package com.pgpcc.project2.aws.message.s3;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "key",
        "size",
        "eTag",
        "versionId",
        "sequencer"
})
@Data
public class S3Object implements Serializable {

    private final static long serialVersionUID = -9107461786125777829L;
    @JsonProperty("key")
    public String key;
    @JsonProperty("size")
    public Integer size;
    @JsonProperty("eTag")
    public String eTag;
    @JsonProperty("versionId")
    public String versionId;
    @JsonProperty("sequencer")
    public String sequencer;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
