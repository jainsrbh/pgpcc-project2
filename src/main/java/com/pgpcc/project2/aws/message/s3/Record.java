package com.pgpcc.project2.aws.message.s3;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "eventVersion",
        "eventSource",
        "awsRegion",
        "eventTime",
        "eventName",
        "userIdentity",
        "requestParameters",
        "responseElements",
        "s3"
})
@Data
public class Record implements Serializable {

    private final static long serialVersionUID = -4342260435679172536L;
    @JsonProperty("eventVersion")
    public String eventVersion;
    @JsonProperty("eventSource")
    public String eventSource;
    @JsonProperty("awsRegion")
    public String awsRegion;
    @JsonProperty("eventTime")
    public String eventTime;
    @JsonProperty("eventName")
    public String eventName;
    @JsonProperty("userIdentity")
    public UserIdentity userIdentity;
    @JsonProperty("requestParameters")
    public RequestParameters requestParameters;
    @JsonProperty("responseElements")
    public ResponseElements responseElements;
    @JsonProperty("s3")
    public S3 s3;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
