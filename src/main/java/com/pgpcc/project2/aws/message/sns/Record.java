
package com.pgpcc.project2.aws.message.sns;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "eventVersion",
    "eventSource",
    "awsRegion",
    "eventTime",
    "eventName",
    "userIdentity",
    "requestParameters",
    "responseElements",
    "s3"
})
@Data
public class Record implements Serializable
{

    @JsonProperty("eventVersion")
    public String eventVersion;
    @JsonProperty("eventSource")
    public String eventSource;
    @JsonProperty("awsRegion")
    public String awsRegion;
    @JsonProperty("eventTime")
    public String eventTime;
    @JsonProperty("eventName")
    public String eventName;
    @JsonProperty("userIdentity")
    public UserIdentity userIdentity;
    @JsonProperty("requestParameters")
    public RequestParameters requestParameters;
    @JsonProperty("responseElements")
    public ResponseElements responseElements;
    @JsonProperty("s3")
    public S3 s3;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();
    private final static long serialVersionUID = 565928751042454484L;

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
