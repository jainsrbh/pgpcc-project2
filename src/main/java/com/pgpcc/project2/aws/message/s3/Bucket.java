package com.pgpcc.project2.aws.message.s3;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "ownerIdentity",
        "arn"
})
@Data
public class Bucket implements Serializable {

    private final static long serialVersionUID = -969484287165725118L;
    @JsonProperty("name")
    public String name;
    @JsonProperty("ownerIdentity")
    public OwnerIdentity ownerIdentity;
    @JsonProperty("arn")
    public String arn;
    @JsonIgnore
    private Map<String, java.lang.Object> additionalProperties = new HashMap<String, java.lang.Object>();

    @JsonAnyGetter
    public Map<String, java.lang.Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, java.lang.Object value) {
        this.additionalProperties.put(name, value);
    }

}
