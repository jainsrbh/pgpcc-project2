
package com.pgpcc.project2.aws.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Type",
        "MessageId",
        "TopicArn",
        "Subject",
        "Message",
        "Timestamp",
        "SignatureVersion",
        "Signature",
        "SigningCertURL",
        "UnsubscribeURL"
})
@Data
public class SNSNotification implements Serializable
{

    @JsonProperty("Type")
    public String type;
    @JsonProperty("MessageId")
    public String messageId;
    @JsonProperty("TopicArn")
    public String topicArn;
    @JsonProperty("Subject")
    public String subject;
    @JsonProperty("Message")
    public String message;
    @JsonProperty("Timestamp")
    public String timestamp;
    @JsonProperty("SignatureVersion")
    public String signatureVersion;
    @JsonProperty("Signature")
    public String signature;
    @JsonProperty("SigningCertURL")
    public String signingCertURL;
    @JsonProperty("UnsubscribeURL")
    public String unsubscribeURL;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -1863130155970458780L;

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
