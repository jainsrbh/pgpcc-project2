package com.pgpcc.project2.utilities;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class HttpUtility {
    public final static String getIpAdress() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public final static String getEndPointUrl(String path, Integer servicePort) throws UnknownHostException {
        return String.format("http://%s:%s/%s", getIpAdress(), servicePort, path);
    }
}
