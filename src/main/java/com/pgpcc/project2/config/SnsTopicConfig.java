package com.pgpcc.project2.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.pgpcc.project2.services.SnsTopicSubscriber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URISyntaxException;

@Configuration
@ConditionalOnProperty(name = "aws.env", havingValue = "ec2")
public class SnsTopicConfig {
    @Bean
    public AmazonSNS snsClient(AWSCredentialsProvider awsCredentialsProvider,
                               @Value("${aws.region}") String region) throws URISyntaxException {
        final AmazonSNSClientBuilder builder = AmazonSNSClient.builder();
        builder.setRegion(region);
        builder.setCredentials(awsCredentialsProvider);
        return builder.build();
    }

    @Bean
    public SnsTopicSubscriber snsTopicSubscriber(@Value("${aws.project2.sns.arn}") String topicArn,
                                                 AmazonSNS snsClient,
                                                 @Value("${server.port}") Integer servicePort) {
        return new SnsTopicSubscriber(snsClient, topicArn, servicePort);
    }
}
