package com.pgpcc.project2.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.pgpcc.project2.repositories.InvoiceRepository;
import com.pgpcc.project2.services.InvoiceCsvWriter;
import com.pgpcc.project2.services.InvoiceFileParser;
import com.pgpcc.project2.services.InvoiceValidator;
import com.pgpcc.project2.services.S3InvoiceService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3Config {
    @Bean
    public AmazonS3 s3Client(@Value("${aws.region}") String region) {
        return AmazonS3ClientBuilder.standard().withRegion(region).build();
    }

    @Bean
    public S3InvoiceService s3InvoiceService(AmazonS3 s3Client, InvoiceRepository invoiceRepository) {
        return new S3InvoiceService(s3Client, new InvoiceFileParser(),
                invoiceRepository, new InvoiceCsvWriter(), new InvoiceValidator());
    }
}
