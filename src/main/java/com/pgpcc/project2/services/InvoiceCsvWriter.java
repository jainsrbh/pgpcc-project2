package com.pgpcc.project2.services;

import com.pgpcc.project2.domain.Invoice;
import lombok.extern.slf4j.Slf4j;

import java.io.FileWriter;
import java.io.IOException;

@Slf4j
public class InvoiceCsvWriter {
    public String writeCsv(Invoice invoice) {
        final String fileName = invoice.getInvoiceId() + ".csv";
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(invoice.csv());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return fileName;
    }
}
