package com.pgpcc.project2.services;

import com.pgpcc.project2.domain.Invoice;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class InvoiceFileParser {
    private static final Pattern CUST_ID = Pattern.compile("^(Customer-ID:)(.*)$");
    private static final Pattern INV_ID = Pattern.compile("^(Inv-ID:)(.*)$");
    private static final Pattern DATE_ID = Pattern.compile("^(Dated:)(.*)$");
    private static final Pattern FROM_ID = Pattern.compile("^(From:)(.*)$");
    private static final Pattern TO_ID = Pattern.compile("^(To:)(.*)$");
    private static final Pattern AMOUNT_ID = Pattern.compile("^(Amount:)(.*)$");
    private static final Pattern SGST_ID = Pattern.compile("^(SGST:)(.*)$");
    private static final Pattern TOTAL_ID = Pattern.compile("^(Total:)(.*)$");
    private static final Pattern INWORDS_ID = Pattern.compile("^(InWords:)(.*)$");
    private static final Pattern ITEM_1_ID = Pattern.compile("^(1\\))(.*)$");
    private static final Pattern ITEM_2_ID = Pattern.compile("^(2\\))(.*)$");
    private static final Pattern ITEM_3_ID = Pattern.compile("^(3\\))(.*)$");
    private static final Pattern ITEM_4_ID = Pattern.compile("^(4\\))(.*)$");
    private static final Pattern ITEM_5_ID = Pattern.compile("^(5\\))(.*)$");

    public Invoice parse(StringBuilder fileText) {
        Invoice invoice = new Invoice();
        invoice.setItems(new ArrayList<>());
        try (Scanner scanner = new Scanner(fileText.toString())) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                log.info("Parsing line : {}", line);
                parseLine(line, invoice);
            }
        }
        return invoice;
    }

    public void parseLine(String line, Invoice invoice) {
        Matcher matcher = CUST_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setCustomerId(matcher.group(2).trim());
        }
        matcher = INV_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setInvoiceId(matcher.group(2).trim());
        }
        matcher = DATE_ID.matcher(line);
        if (matcher.matches()) {
            try {
                invoice.setDate(new SimpleDateFormat("MMM dd yyyy").parse(matcher.group(2).trim()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        matcher = FROM_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setFrom(matcher.group(2).trim());
        }
        matcher = TO_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setTo(matcher.group(2).trim());
        }
        matcher = AMOUNT_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setAmount(new BigDecimal(matcher.group(2).trim()));
        }
        matcher = SGST_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setSgst(new BigDecimal(matcher.group(2).trim()));
        }
        matcher = TOTAL_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setTotal(new BigDecimal(matcher.group(2).trim()));
        }
        matcher = INWORDS_ID.matcher(line);
        if (matcher.matches()) {
            invoice.setTotalInWords(matcher.group(2).trim());
        }
        matcher = ITEM_1_ID.matcher(line);
        if (matcher.matches()) {
            invoice.getItems().add(matcher.group(2).trim());
        }
        matcher = ITEM_2_ID.matcher(line);
        if (matcher.matches()) {
            invoice.getItems().add(matcher.group(2).trim());
        }
        matcher = ITEM_3_ID.matcher(line);
        if (matcher.matches()) {
            invoice.getItems().add(matcher.group(2).trim());
        }
        matcher = ITEM_4_ID.matcher(line);
        if (matcher.matches()) {
            invoice.getItems().add(matcher.group(2).trim());
        }
        matcher = ITEM_5_ID.matcher(line);
        if (matcher.matches()) {
            invoice.getItems().add(matcher.group(2).trim());
        }
    }
}
