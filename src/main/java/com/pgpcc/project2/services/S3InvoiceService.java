package com.pgpcc.project2.services;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.sns.message.SnsNotification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pgpcc.project2.aws.message.SNSNotification;
import com.pgpcc.project2.aws.message.sns.Record;
import com.pgpcc.project2.aws.message.sns.Records;
import com.pgpcc.project2.domain.Invoice;
import com.pgpcc.project2.repositories.InvoiceRepository;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class S3InvoiceService {
    private final AmazonS3 amazonS3;
    private final InvoiceFileParser invoiceFileParser;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceCsvWriter invoiceCsvWriter;
    private final InvoiceValidator invoiceValidator;

    public S3InvoiceService(AmazonS3 amazonS3,
                            InvoiceFileParser invoiceFileParser,
                            InvoiceRepository invoiceRepository,
                            InvoiceCsvWriter invoiceCsvWriter,
                            InvoiceValidator invoiceValidator) {
        this.amazonS3 = amazonS3;
        this.invoiceFileParser = invoiceFileParser;
        this.invoiceRepository = invoiceRepository;
        this.invoiceCsvWriter = invoiceCsvWriter;
        this.invoiceValidator = invoiceValidator;
    }

    public List<Invoice> processInvoice(Records records) {
        List<Invoice> invoices = new ArrayList<>();
        for(Record record: records.records){
            invoices.add(processInvoice(record));
        }
        return invoices;
    }

    public Invoice processInvoice(Record message) {
        log.info("New record: [{}]",message);
        String bucketName = message.getS3().  getBucket().getName();
        String keyName = message.getS3().getObject().getKey();
        log.info("New event triggered from bucket : {} with new object : {}",bucketName, keyName);
        return this.processInvoice(bucketName, keyName);
    }

    public Invoice processInvoice(String bucketName, String keyName) {
        S3Object s3Object = amazonS3.getObject(bucketName, keyName);
        log.info("Reading file : {}/{}", s3Object.getBucketName(), s3Object.getKey());
        StringBuilder sb = new StringBuilder();
        try (S3ObjectInputStream s3is = s3Object.getObjectContent()) {
            byte[] read_buf = new byte[1024];
            int read_len = 0;
            while ((read_len = s3is.read(read_buf)) > 0) {
                sb.append(new String(read_buf));
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        Invoice invoice = this.invoiceFileParser.parse(sb);
        if(invoiceValidator.isValid(invoice)){
            invoice = this.invoiceRepository.save(invoice);
            log.info("Saved to database: {}", invoice);
            final String csvFileName = this.invoiceCsvWriter.writeCsv(invoice);
            final PutObjectResult csvUploadResult = amazonS3.putObject(bucketName + "/csv-invoices", csvFileName, new File(csvFileName));
            log.info("Invoice csv upload result: {}", csvUploadResult.getMetadata().getRawMetadata());
        }else{
            log.warn("Invalid invoice, not processed: [{}]",invoice.toString());
        }
        return invoice;
    }

    private String parseSnsMessage(SnsNotification snsNotification){
        String message = snsNotification.getMessage().replaceAll("\\\"","\"");
        return message;
    }

    public static void main(String[] args) throws JsonProcessingException {
        String m = "\"{\\\"Records\\\":[{\\\"eventVersion\\\":\\\"2.1\\\",\\\"eventSource\\\":\\\"aws:s3\\\",\\\"awsRegion\\\":\\\"us-east-1\\\",\\\"eventTime\\\":\\\"2020-08-02T05:01:04.849Z\\\",\\\"eventName\\\":\\\"ObjectCreated:Put\\\",\\\"userIdentity\\\":{\\\"principalId\\\":\\\"AV8N9U6CCXHQE\\\"},\\\"requestParameters\\\":{\\\"sourceIPAddress\\\":\\\"218.191.19.197\\\"},\\\"responseElements\\\":{\\\"x-amz-request-id\\\":\\\"5F2951A9DF1E6C4B\\\",\\\"x-amz-id-2\\\":\\\"TowsbhkXcgdjwWI4tejJnEUzOa01vWafWWCJ24CaxgRj+gigtH7pv75E24L3Pse1zziWOFSM7w1f8q4D6md5f/5Cj0YS9XxW\\\"},\\\"s3\\\":{\\\"s3SchemaVersion\\\":\\\"1.0\\\",\\\"configurationId\\\":\\\"sj-project2-s3-upload\\\",\\\"bucket\\\":{\\\"name\\\":\\\"sj-project2-s3\\\",\\\"ownerIdentity\\\":{\\\"principalId\\\":\\\"AV8N9U6CCXHQE\\\"},\\\"arn\\\":\\\"arn:aws:s3:::sj-project2-s3\\\"},\\\"object\\\":{\\\"key\\\":\\\"invoice2.txt\\\",\\\"size\\\":569,\\\"eTag\\\":\\\"7c86707644b3fad4a1b3db97a4334aa3\\\",\\\"versionId\\\":\\\"Pasx_Sau2q1.3kUStclPMT_gumj1hsRm\\\",\\\"sequencer\\\":\\\"005F26489887F10733\\\"}}}]}\"";
        String r = m.replaceAll("\\\\","");
        r = r.replaceFirst("\"","");
        r = r.substring(0,r.length()-1);
        System.out.println(r);
        ObjectMapper objectMapper = new ObjectMapper();
        Record record = objectMapper.readerFor(Record.class).readValue(r);

        System.out.println(record);
    }
}
