package com.pgpcc.project2.repositories;

import com.pgpcc.project2.domain.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, String> {
    Optional<Invoice> findById(String id);
}
